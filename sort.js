const arrayRandom = [4, 9, 7, 5, 8, 9, 3];

function swap(array, i, j) {
    let temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }

function bubbleSort(array) {
    let countOuter = 0;
    let countSwap = 0;

    let swapped;
    do {
      countOuter++;
      swapped = false;
      for(let i = 0; i < array.length; i++) {
        if(array[i] && array[i + 1] && array[i] > array[i + 1]) {
          countSwap++;
          swap(array, i, i + 1);
          swapped = true;
        }
      }
    } while(swapped);
  
    console.log('Jumlah swap: ', countSwap);
    return array;
}

bubbleSort(arrayRandom);
console.log(arrayRandom);
  